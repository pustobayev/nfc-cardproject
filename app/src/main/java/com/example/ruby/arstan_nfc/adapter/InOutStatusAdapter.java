package com.example.ruby.arstan_nfc.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ruby.arstan_nfc.NFCApplication;
import com.example.ruby.arstan_nfc.R;
import com.example.ruby.arstan_nfc.utils.Listdata;

import java.util.ArrayList;

/**
 * Created by jina on 10/24/2017.
 */

public class InOutStatusAdapter extends BaseAdapter{

    Context context;
    NFCApplication application;
    private static LayoutInflater inflater = null;
    ArrayList<Listdata> array;

    public InOutStatusAdapter(final Context context, ArrayList<Listdata> arrayList)
    {

        this.context = context;
        this.array = arrayList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        application = (NFCApplication) context.getApplicationContext();

        View v = inflater.inflate(R.layout.listview_cell, null);



    }

    @Override
    public int getCount() {
//        if (application.listdatas.size() == null){
//
//        }
        return array.size();

    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi=view;
        vi = inflater.inflate(R.layout.listview_cell, null);
        TextView name = vi.findViewById(R.id.list_name);
        name.setText(array.get(i).name);

        TextView state = vi.findViewById(R.id.list_state);
        ImageView img = vi.findViewById(R.id.list_stateimage);
        if (array.get(i).action.equals("1")){
            state.setText("в школе");
            if (array.get(i).gender.equals("Femail")){
                Drawable d = context.getDrawable(R.drawable.coestate_w);
                img.setImageDrawable(d);
            }else{
                Drawable d = context.getDrawable(R.drawable.comestate_m);
                img.setImageDrawable(d);
            }
        }else{
            state.setText("вышел из школы");
            if (array.get(i).gender.equals("Femail")){
                Drawable d = context.getDrawable(R.drawable.gostate_w);
                img.setImageDrawable(d);
            }else {
                Drawable d = context.getDrawable(R.drawable.gostate_m);
                img.setImageDrawable(d);
            }
        }

        TextView time = vi.findViewById(R.id.list_date);
        time.setText(array.get(i).date);
        return vi;
    }
}
