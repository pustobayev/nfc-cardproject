package com.example.ruby.arstan_nfc;

import android.content.Context;

import com.example.ruby.arstan_nfc.utils.Listdata;
import com.example.ruby.arstan_nfc.utils.Logindata;
import com.example.ruby.arstan_nfc.utils.School;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Ruby on 6/17/2017.
 */

public class APIManager {

    interface MyCallBackInterface {
        void onSuccess(JSONObject result) throws JSONException;
        void onFailure(String error, int nCode);
    }

    NFCApplication application;
    

    private static APIManager instance;
    OkHttpClient client;
    private APIManager() { client = new OkHttpClient();}

    public static APIManager getInstance(Context context) {
        if (instance == null) {
            instance = new APIManager();
        }
        instance.application = (NFCApplication) context.getApplicationContext();
        return instance;
    }

    public void getSchools(final MyCallBackInterface callback){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("access_token", "sw0o048wkw0g4kwkc0s4484w0wwsssc804cs48wk")
                .url("https://main.api.net.kg/api/school")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFailure("Faild", 500);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonData = response.body().string();
                int code = response.code();

                try {
                    JSONObject object = new JSONObject(jsonData);
                    application.schools = new ArrayList<School>();
                    JSONArray array = object.getJSONArray("result");
                    for(int i = 0; i < array.length(); i++){
                        JSONObject obj = array.getJSONObject(i);
                        School school = new School(obj);
                        application.schools.add(school);
                    }
                    if (code == 201 || code == 200){
                        callback.onSuccess(object);
                    }
                    else{
                        callback.onFailure("Network Erro", code);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }
    public void login(String phone, String password, final String url, final MyCallBackInterface callback){
        OkHttpClient client = new OkHttpClient();

        RequestBody body = new FormBody.Builder()
                .add("phone", phone)
                .add("password", password)
                .build();

        String strUrl = String.format("%s%s", url, "/login/guard");

        final Request request = new Request.Builder()
                .url(strUrl)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFailure("Faild", 500);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonData = response.body().string();
                int code = response.code();
                try {
                    JSONObject object = new JSONObject(jsonData);
                    JSONObject data = object.getJSONObject("data");
                    Logindata login = new Logindata(data, url);
                    application.logindata = login;



                    if (code == 201 || code == 200){
                        callback.onSuccess(data);
                    }
                    else{
                        callback.onFailure("Network Erro", code);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (code != 201 || code != 200){
                    callback.onFailure("Network Error", code);
                }
            }
        });
    }

    public void getCardDetail(String cardId, final MyCallBackInterface callback){
        OkHttpClient client = new OkHttpClient();

        String strUrl = String.format("%s%s%s",application.logindata.api, "/identity/?nfc_id=", cardId);

        final Request request = new Request.Builder()
                .header("access_token", application.logindata.access_token)
                .url(strUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFailure("Faild", 500);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String json = response.body().string();
                int code = response.code();
                JSONObject object = null;

                if (code == 201 || code == 200){
                    try {
                        object = new JSONObject(json);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        callback.onSuccess(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else{
                        callback.onFailure("Network Erro", code);
                }


            }
        });

    }

    public void getListData(String start, String end, final MyCallBackInterface callback){
        OkHttpClient client = new OkHttpClient();
        String strUrl = String.format("%s%s%s%s", application.logindata.api, "/attendance?start_date=", start, "&end_date=", end);

        Request request = new Request.Builder()
                .header("access_token", application.logindata.access_token)
                .url(strUrl)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFailure("Faild", 500);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String json = response.body().string();
                int code = response.code();
                JSONObject object = null;
                try {
                    object = new JSONObject(json);
                    application.listdatas = new ArrayList<Listdata>();
                    JSONArray array = object.getJSONArray("result");
                    for(int i = 0; i < array.length(); i++){
                        JSONObject obj = array.getJSONObject(i);
                        Listdata data = new Listdata(obj);
                        application.listdatas.add(data);
                    }
                    if (code == 201 || code == 200){
                        callback.onSuccess(object);
                    }else{
                        callback.onFailure("Network Error", code);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    public  void putCardsData(JSONArray array, final MyCallBackInterface callback){
        OkHttpClient client = new OkHttpClient();

        String strUrl = String.format("%s%s", application.logindata.api, "/attendance");

        RequestBody body = RequestBody.create(JSON, array.toString());
        final Request request = new Request.Builder()
                .header("access_token", application.logindata.access_token)
                .url(strUrl)
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFailure("Faild", 500);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                int code = response.code();
                String json = response.body().string();
                try {
                    JSONObject object = new JSONObject(json);
                    if (code == 201 || code == 200){
                        callback.onSuccess(object);
                    }else{
                        callback.onFailure("Network Error", code);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }
    public void getAccess_Token(String name, String password, final MyCallBackInterface callback) throws IOException{
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("username", name)
                .add("password",password)
                .build();

        final Request request = new Request.Builder()
                .header("Content-Type","application/x-www-form-urlencoded")
                .url("https://qauss.api.net.kg/login/")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonData = response.body().string();
                int code = response.code();
                try {

                    JSONObject object = new JSONObject(jsonData);
                    //JSONArray array = object.getJSONArray("results");
                    if(code == 201 || code == 200){
                        if(object.getString("status")=="true")
                            callback.onSuccess(object);
                        else
                        {
                            callback.onFailure("Network Error",code);
                        }
                    }
                    else {
                        callback.onFailure(object.getString("errors"), code);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onFailure("Network Error", code);
                }
            }
        });
    }
    public void getStudentDetail(String access_token, String nfc_tag_id, final MyCallBackInterface callback) throws IOException{
        OkHttpClient client = new OkHttpClient();

        final Request request = new Request.Builder()
                .header("access_token",access_token)
                .url("https://qauss.api.net.kg/identify/"+nfc_tag_id)
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonData = response.body().string();
                int code = response.code();
                try {

                    JSONObject object = new JSONObject(jsonData);
                    //JSONArray array = object.getJSONArray("results");
                    if(code == 200){
                        if(object.getString("status")=="true")
                            callback.onSuccess(object);
                        else
                        {
                            callback.onFailure("Network Error",code);
                        }
                    }
                    else {
                        callback.onFailure(object.getString("errors"), code);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onFailure("Network Error", code);
                }
            }
        });
    }
    public void request_enter(String access_token, String nfc_tag_id, final MyCallBackInterface callback) throws IOException{
        OkHttpClient client = new OkHttpClient();

        final Request request = new Request.Builder()
                .header("access_token",access_token)
                .url("https://qauss.api.net.kg/attendance/in/"+nfc_tag_id)
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonData = response.body().string();
                int code = response.code();
                try {

                    JSONObject object = new JSONObject(jsonData);
                    //JSONArray array = object.getJSONArray("results");
                    if(code == 200 || code == 201){
                        if(object.getString("status")=="true")
                            callback.onSuccess(object);
                        else
                        {
                            callback.onFailure("Network Error",code);
                        }
                    }
                    else {
                        callback.onFailure(object.getString("errors"), code);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onFailure("Network Error", code);
                }
            }
        });
    }
    public void request_exit(String access_token, String nfc_tag_id, final MyCallBackInterface callback) throws IOException{
        OkHttpClient client = new OkHttpClient();

        final Request request = new Request.Builder()
                .header("access_token",access_token)
                .url("https://qauss.api.net.kg/attendance/out/"+nfc_tag_id)
                .get()
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonData = response.body().string();
                int code = response.code();
                try {

                    JSONObject object = new JSONObject(jsonData);
                    //JSONArray array = object.getJSONArray("results");
                    if(code == 200 || code == 201){
                        if(object.getString("status")=="true")
                            callback.onSuccess(object);
                        else
                        {
                            callback.onFailure("Network Error",code);
                        }
                    }
                    else {
                        callback.onFailure(object.getString("errors"), code);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onFailure("Network Error", code);
                }
            }
        });
    }
}
