package com.example.ruby.arstan_nfc.utils;

import org.json.JSONObject;

/**
 * Created by jina on 10/23/2017.
 */

public class School {

    public String id;
    public String name;
    public String domain;
    public String api;
    public String date;
    public String status;
    public String logo;

    public  School(JSONObject json){
        id = json.optString("id","");
        name = json.optString("name", "");
        domain = json.optString("domain","");
        api = json.optString("api","");
        date = json.optString("date", "");
        status = json.optString("status", "");
        logo = json.optString("logo", "");
    }
}
