package com.example.ruby.arstan_nfc;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {
    SharedPreferences sharedPref;
    SharedPreferences sharedPref_1;

    private PendingIntent pendingIntent;
    private IntentFilter[] intentFiltersArray;
    private String[][] techListsArray;
    private Intent i;
    public Intent confirmintent;
    TextView tf_TagID;
    TextView tv_count;
    TextView tv_timer;
    String group,firstname,lastname;
    public int action = 1;
    NFCApplication application;
    CountDownTimer countDownTimer;
    static int mins;
    static int secs;


    DrawerLayout mDrawer;
    private Toolbar toolbar;

    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    private NdefMessage mNdefPushMessage;

    private AlertDialog mDialog;

    private List<Tag> mTags = new ArrayList<Tag>();
    String access_token;

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        application = (NFCApplication) getApplication();

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        Intent intent = getIntent();
        //access_token = intent.getStringExtra("access_token");


        sharedPref = this.getSharedPreferences("shared", Context.MODE_PRIVATE);
        sharedPref_1 = this.getSharedPreferences("listdata", Context.MODE_PRIVATE);
        String access_ =  sharedPref.getString(getString(R.string.access_token), "false");

        countDownTimer = new CountDownTimer(300000, 1000) {
            @Override
            public void onTick(long l) {
                String str = String.format("%d:%02d", mins, secs);
                tv_timer.setText(str);
                if (secs == 0){

                    secs = 59;
                    mins -= 1;
                }else{
                    secs -= 1;
                }
            }

            @Override
            public void onFinish() {
                try {
                    putData();
                    secs = 59;
                    mins = 4;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        //getData(getIntent());



        i = new Intent(MainActivity.this,ProfileActivity.class);
        confirmintent = new Intent(MainActivity.this, ConfirmActivity.class);


//        tf_TagID = (TextView)findViewById(R.id.NFCTagID);
        access_token=access_;
        //i.putExtra("tagID",myTag.getId().toString());
        onNewIntent(getIntent());

        mDialog = new AlertDialog.Builder(this).setNeutralButton("Ok", null).create();

        mAdapter = NfcAdapter.getDefaultAdapter(this);
//        if (mAdapter == null) {
////            showMessage(R.string.error, R.string.nfc_error);
//            //finish();
//            return;
//        }

        mPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        mNdefPushMessage = new NdefMessage(new NdefRecord[] { newTextRecord(
                "Message from NFC Reader :-)", Locale.ENGLISH, true) });

//        getCardDetail("8037390A188204");
//        getCardDetail("445566");

        LinearLayout listlayer = (LinearLayout) findViewById(R.id.layer_listview);
        listlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListviewActivity.class);
                startActivity(intent);
            }
        });

        int count = sharedPref_1.getInt("count", 0);
        tv_count = (TextView) findViewById(R.id.main_count);
        String str1 = "(" + count + ")";
        tv_count.setText(str1);
        tv_timer = (TextView) findViewById(R.id.main_timer);

        //------ set Menu view
        TextView schoolname = (TextView) findViewById(R.id.school_name);
        schoolname.setText(sharedPref.getString("schoolname", ""));

        CircleImageView image = (CircleImageView) findViewById(R.id.school_image);
        int lenth = application.logindata.api.length();
        String api = application.logindata.api.substring(0, lenth - 4);
        String photo = sharedPref.getString("logo", "");
        String url = String.format("%s%s%s", api, "/assets/img/", photo);
        Picasso.with(this).load(url).into(image);
        //-------------//



        final Button in = (Button) findViewById(R.id.btn_in);
        final Button out = (Button) findViewById(R.id.btn_out);
        in.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                application.action = 1;
                in.setBackgroundResource(R.drawable.button1_1);
                out.setBackgroundResource(R.drawable.button2_2);
//                in.setBackgroundColor(0xFF00B3C0);
//                out.setBackgroundColor(0xFFF8743A);
            }
        });
        out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                application.action = 2;
                in.setBackgroundResource(R.drawable.button1_2);
                out.setBackgroundResource(R.drawable.button2_1);
//                out.setBackgroundColor(0xFF00B3C0);
//                in.setBackgroundColor(0xFFF8743A);
            }
        });

        if (application.action == 1){
            in.setBackgroundResource(R.drawable.button1_1);
            out.setBackgroundResource(R.drawable.button2_2);
//            in.setBackgroundColor(0xFF00B3C0);
//            out.setBackgroundColor(0xFFF8743A);
        }else{
            in.setBackgroundResource(R.drawable.button1_2);
            out.setBackgroundResource(R.drawable.button2_1);
//            out.setBackgroundColor(0xFF00B3C0);
//            in.setBackgroundColor(0xFFF8743A);
        }
    }

    @Override
    public void onBackPressed() {

    }

    private void showMessage(int title, int message) {
        mDialog.setTitle(title);
        mDialog.setMessage(getText(message));
//        mDialog.show();
    }

    private NdefRecord newTextRecord(String text, Locale locale, boolean encodeInUtf8) {
        byte[] langBytes = locale.getLanguage().getBytes(Charset.forName("US-ASCII"));

        Charset utfEncoding = encodeInUtf8 ? Charset.forName("UTF-8") : Charset.forName("UTF-16");
        byte[] textBytes = text.getBytes(utfEncoding);

        int utfBit = encodeInUtf8 ? 0 : (1 << 7);
        char status = (char) (utfBit + langBytes.length);

        byte[] data = new byte[1 + langBytes.length + textBytes.length];
        data[0] = (byte) status;
        System.arraycopy(langBytes, 0, data, 1, langBytes.length);
        System.arraycopy(textBytes, 0, data, 1 + langBytes.length, textBytes.length);

        return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], data);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        getCardDetail("8037390A188204");
        mDrawer.closeDrawers();
        if (mAdapter != null) {
            if (!mAdapter.isEnabled()) {
                showWirelessSettingsDialog();
            }
            mAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
//            mAdapter.enableForegroundNdefPush(this, mNdefPushMessage);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAdapter != null) {
            mAdapter.disableForegroundDispatch(this);
//            mAdapter.disableForegroundNdefPush(this);
        }
    }


    private void showWirelessSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.nfc_disabled);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.create().show();
        return;
    }

    @Override
    public void onNewIntent(final Intent intent) {
        int count = sharedPref_1.getInt("count", 0);
        if (count == 30)
            return;
        setIntent(intent);


//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
                getData(intent);
//            }
//        });
//        try {
//            APIManager.getInstance(this).getStudentDetail(access_token, "80497d3a992d04", new APIManager.MyCallBackInterface() {
//                @Override
//                public void onSuccess(JSONObject result) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                        }
//                    });
//
//                }
//
//                @Override
//                public void onFailure(String error, int nCode) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
////                            new SweetAlertDialog(MainActivity.this, SweetAlertDialog.PROGRESS_TYPE)
////                                    .setTitleText("Error!")
////                                    .setContentText("Token Changed!")
////                                    .show();
////                            finish();
//                        }
//                    });
//                }
//            });
//        } catch (IOException e) {
//            e.printStackTrace();
//        }



    }
    private void getData(Intent intent) {
        // NFC Background
        String action = intent.getAction();

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

//            Toast.makeText(this,"Card discovered", Toast.LENGTH_SHORT).show();
//            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
//            NdefMessage[] msgs;
//            if (rawMsgs != null) {
//                msgs = new NdefMessage[rawMsgs.length];
//                for (int i = 0; i < rawMsgs.length; i++) {
//                    msgs[i] = (NdefMessage) rawMsgs[i];
//                }
//            } else {
//                // Unknown tag type
//                byte[] empty = new byte[0];
//                byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
//                Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//                byte[] payload = dumpTagData(tag).getBytes();
//                NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, id, payload);
//                NdefMessage msg = new NdefMessage(new NdefRecord[] { record });
//                msgs = new NdefMessage[] { msg };
//                mTags.add(tag);
//            }
//            // Setup the views
//            buildTagViews(msgs);
            Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            handleNfc(tag);
        }

//        if(Build.VERSION.SDK_INT >= 10) {
//            if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {
//                Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//                handleNfc(tagFromIntent);
//            }
//        }
    }

    private void handleNfc(Tag tag) {
        if(Build.VERSION.SDK_INT >= 10) {
            final String TagUID = ConvertbyteArrayToHexString(tag.getId());

//            tf_TagID.setText(TagUID);
            //tvNFCID.setText(TagUID);


//            Toast.makeText(this, TagUID, Toast.LENGTH_SHORT).show();
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
                    getCardDetail(TagUID);
//                }
//            });


//            APIManager.getInstance(this).getCardDetail(TagUID, new APIManager.MyCallBackInterface() {
//                @Override
//                public void onSuccess(JSONObject result) {
//
//                    Handler mainHandler = new Handler(MainActivity.this.getMainLooper());
//
//                    Runnable myRunnable = new Runnable() {
//                        @Override
//                        public void run() {
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    notifySuccessfulScan();
//                                }
//                            });
////                                    tf_st_group.setText(group);
////                                    tf_st_name.setText(firstname+" "+lastname);
//                        } // This is your code
//                    };
//                    mainHandler.post(myRunnable);
//                }
//
//                @Override
//                public void onFailure(String error, int nCode) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
//                                    .setTitleText("Error!")
//                                    .setContentText("Card Info is Wrong!")
//                                    .show();
//                        }
//                    });
//                }
//            });


        }
    }
    public void getCardDetail(String id){
        Toast.makeText(this, id, Toast.LENGTH_SHORT).show();
        int i = 0;
        final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.success);
        final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.error);
        APIManager.getInstance(this).getCardDetail(id, new APIManager.MyCallBackInterface() {
            @Override
            public void onSuccess(final JSONObject result) throws JSONException {



//                Intent intent = new Intent(MainActivity.this, ConfirmActivity.class);
//                startActivity(intent);
                //                    group = result.getString("group");
//                    firstname = result.getString("first_name");
//                    lastname = result.getString("last_name");
                try {
                    int count = sharedPref_1.getInt("count", 0);
                    count++;
                    SharedPreferences.Editor editor = sharedPref_1.edit();
                    Gson gson = new Gson();
                    String json = gson.toJson(result);
                    String str = String.format("%s%d", "item", count);
                    editor.putString(str, json);
                    editor.putInt("count", count);
                    String currentTimeString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                    str = String.format("%s%d", "date", count);
                    editor.putString(str, currentTimeString);
                    str = String.format("%s%d", "action", count);
                    editor.putString(str, String.format("%d", application.action));

                    editor.commit();

                    final int printcount = count;

                    if (count == 1){
                        secs = 59;
                        mins = 4;
                        countDownTimer.start();
                    }

//                    try {
//                        putData();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }

                    if (count == 30) {
                        try {
                            countDownTimer.cancel();
                            putData();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    JSONObject a = null;
                    try {
                        a = result.getJSONObject("data");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String fir = a.optString("first_name", "");
                    String last = a.optString("last_name", "");
                    String photo = a.optString("photo", "");
                    String role =  result.optString("role", "");
                    String classname;
                    if (role.equals("Student")){
                        classname = a.optString("class", "");
                    }else{
                        classname = a.optString("department", "");
                    }
                    confirmintent.putExtra("name", String.format("%s %s", last, fir));
                    confirmintent.putExtra("photo", photo);
                    confirmintent.putExtra("class", classname);

//                        Intent intent = new Intent(MainActivity.this, ConfirmActivity.class);

//                        notifySuccessfulScan();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                                notifySuccessfulScan();
                            String str1 = "(" + printcount + ")";
                            tv_count.setText(str1);
                            mp1.start();
                            startActivity(confirmintent);

                        }
                    });
//                                    tf_st_group.setText(group);
//                                    tf_st_name.setText(firstname+" "+lastname);
                }catch (final Exception e){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            StringWriter sw = new StringWriter();
                            PrintWriter pw = new PrintWriter(sw);
                            e.printStackTrace(pw);
                            String sStackTrace = sw.toString(); // stack trace as a string

                            Toast.makeText(MainActivity.this, sStackTrace, Toast.LENGTH_LONG).show();

                        }
                    });

                }


            }

            @Override
            public void onFailure(String error, int nCode) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mp2.start();
                        final SweetAlertDialog alert = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE);
//                        alert.setTitleText("Error!");
                        alert.setTitleText("ошибка!");
//                        alert.setContentText("Card Info is Wrong!");
                        alert.setContentText("карта не зарегистрирована");
                        alert.show();
                        alert.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        new CountDownTimer(1000, 1000) {

                            public void onTick(long millisUntilFinished) {
                                //                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                            }

                            public void onFinish() {
                                //                mTextField.setText("done!");
                                alert.hide();
                            }
                        }.start();
                    }
                });
            }
        });
    }
    public void putData() throws JSONException {
        Gson gson = new Gson();
        JSONArray array = new JSONArray();
        for (int i = 1; i <= sharedPref_1.getInt("count", 0); i ++){
            String str = String.format("%s%d", "item", i);
            String json = sharedPref_1.getString(str, "");
            JSONObject object = gson.fromJson(json, JSONObject.class);
            String role = object.optString("role", "");
            String strData = object.optString("data","");
            JSONObject data = gson.fromJson(strData, JSONObject.class);
            String user_id = String.format("%d",data.optInt("student_id", -1));
            String parent_id = String.format("%d", data.optInt("parent_id", -1));
            String nfc_id = data.optString("nfc_id", "");
            str = String.format("%s%d", "date", i);
            String date = sharedPref_1.getString(str, "");

            str = String.format("%s%d", "action", i);
            String act = sharedPref_1.getString(str, "");

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("user_id", user_id);
            jsonParam.put("parent_id", parent_id);
            jsonParam.put("nfc_id", nfc_id);
            jsonParam.put("date", date);
            jsonParam.put("action", act);
            jsonParam.put("role", role);
            array.put(jsonParam);
        }


        APIManager.getInstance(this).putCardsData(array, new APIManager.MyCallBackInterface() {
            @Override
            public void onSuccess(JSONObject result) throws JSONException {
                int i = 0;
                SharedPreferences.Editor editor = sharedPref_1.edit();
                editor.clear();
                editor.commit();
            }

            @Override
            public void onFailure(String error, int nCode) {
                int i = 0;
            }
        });
//        JSONObject data1 = new JSONObject();
//        JSONObject attributes = new JSONObject();
//        attributes.put("text", txt);
//
//        data1.put("attributes",attributes);
//        data1.put("type","comments");
//        jsonParam.put("data",data1);


    }
    private void notifySuccessfulScan() {
     //   setVolumeControlStream(AudioManager.STREAM_MUSIC);

     //   MediaPlayer mp = MediaPlayer.create(this, R.raw.scan);
        Vibrator v = (Vibrator) getSystemService(MainActivity.VIBRATOR_SERVICE);

     //   mp.start();
        v.vibrate(1000);


        i.putExtra("group",group);
        i.putExtra("name",firstname+lastname);
        startActivity(i);
    }
    public static String ConvertbyteArrayToHexString(byte in[]) {
        byte ch = 0x00;
        int i = in.length-1;

        if (in == null) {
            return null;
        }

        String HEXSET[] = {"0", "1", "2","3", "4", "5", "6", "7", "8","9", "A", "B", "C", "D", "E","F"};

        //Double length, as you're converting an array of 8 bytes, to 16 characters for hexadecimal
        StringBuffer out = new StringBuffer(in.length * 2);

        //You need to iterate from msb to lsb, in the case of using iCode SLI rfid
        while (i >= 0) {
            ch = (byte) (in[i] & 0xF0); // Strip off high nibble
            ch = (byte) (ch >>> 4); // shift the bits down
            ch = (byte) (ch & 0x0F); // must do this is high order bit is on!
            out.append(HEXSET[ (int) ch]); // convert the nibble to a String Character
            ch = (byte) (in[i] & 0x0F); // Strip off low nibble
            out.append(HEXSET[ (int) ch]); // convert the nibble to a String Character
            i--;
        }
        return (new String(out));
    }

}
