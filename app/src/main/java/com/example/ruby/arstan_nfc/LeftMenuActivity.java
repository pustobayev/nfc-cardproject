package com.example.ruby.arstan_nfc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class LeftMenuActivity extends AppCompatActivity {

    LinearLayout home;
    LinearLayout listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_left_menu);

        home = (LinearLayout) findViewById(R.id.layer_home);
        listview = (LinearLayout) findViewById(R.id.layer_listview);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = 0;
                Intent intent = new Intent(LeftMenuActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        listview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LeftMenuActivity.this, ListviewActivity.class);
                startActivity(intent);
            }
        });
    }
}
