package com.example.ruby.arstan_nfc.utils;

import org.json.JSONObject;

/**
 * Created by jina on 10/23/2017.
 */

public class Logindata {
    public String status;
    public String access_token;
    public String id;
    public String first_name;
    public String last_name;
    public String role;
    public String api;

    public Logindata(JSONObject jsonObject, String url){
        status = jsonObject.optString("status","");
        access_token = jsonObject.optString("access_token", "");
        id = jsonObject.optString("ID", "");
        first_name = jsonObject.optString("first_name", "");
        last_name = jsonObject.optString("last_name", "");
        role = jsonObject.optString("role", "");
        api = url;
    }
}
