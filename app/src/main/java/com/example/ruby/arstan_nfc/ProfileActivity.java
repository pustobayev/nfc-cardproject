package com.example.ruby.arstan_nfc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ProfileActivity extends AppCompatActivity {

    TextView tf_st_group;
    TextView tf_st_name;
    ImageButton btn_enter;
    ImageButton btn_exit;
    ImageView img_success;
    SweetAlertDialog dialog;
    String access_token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Intent intent = getIntent();

        String tf_name = intent.getStringExtra("name");
        final String tagID;

        tagID = intent.getStringExtra("tagID");
        SharedPreferences sharedPref = this.getSharedPreferences("shared", Context.MODE_PRIVATE);
        access_token =  sharedPref.getString(getString(R.string.access_token), "false");
        //access_token = intent.getStringExtra("access_token");

        String tf_group = intent.getStringExtra("group");

        tf_st_group = (TextView)findViewById(R.id.tf_st_group);
        tf_st_name = (TextView)findViewById(R.id.tf_st_name);

        tf_st_name.setText(tf_name);
        tf_st_group.setText(tf_group);

        btn_enter = (ImageButton)findViewById(R.id.btn_enter);
        btn_exit = (ImageButton)findViewById(R.id.btn_exit);
        img_success = (ImageView)findViewById(R.id.img_success);
        dialog = new SweetAlertDialog(ProfileActivity.this, SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Welcome!")
                .setContentText("");


        btn_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                try {
                    APIManager.getInstance(ProfileActivity.this).request_enter(access_token, tagID, new APIManager.MyCallBackInterface() {
                        @Override
                        public void onSuccess(JSONObject result) {
                            try {
                                final String msg = result.getString("message");

                                Handler mainHandler = new Handler(ProfileActivity.this.getMainLooper());

                                final View view=img_success;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                dialog.show();
                                            }
                                        });
                                        img_success.setVisibility(View.VISIBLE);

                                    }
                                });
                                view.postDelayed(new Runnable() { public void run() { view.setVisibility(View.VISIBLE);
                                    dialog.dismiss();
                                    onBackPressed();
                                } }, 2000);
                               /* Runnable myRunnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        img_success.setVisibility(View.VISIBLE);
                                        Intent intent = new Intent(ProfileActivity.this, LoginActivity.class );
                                        intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                                        startActivity( intent );
                                    } // This is your code
                                };
                                //mainHandler.post(myRunnable);
                                mainHandler.postDelayed(myRunnable,1000);*/
                                //img_success.setVisibility(View.VISIBLE);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(String error, int nCode) {
                            view.postDelayed(new Runnable() { public void run() {
                                new SweetAlertDialog(ProfileActivity.this, SweetAlertDialog.PROGRESS_TYPE)
                                        .setTitleText("Error!")
                                        .setContentText("Token Changed")
                                        .show();
                                dialog.dismiss();
                                Intent intent = new Intent(ProfileActivity.this,LoginActivity.class);
                                startActivity(intent);

                            } }, 100);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }



            }
        });

        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                try {
                    APIManager.getInstance(ProfileActivity.this).request_exit(access_token, tagID, new APIManager.MyCallBackInterface() {
                        @Override
                        public void onSuccess(JSONObject result) {
                            try {
                                final String msg = result.getString("message");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        new SweetAlertDialog(ProfileActivity.this, SweetAlertDialog.PROGRESS_TYPE)
                                                .setTitleText("Welcome!")
                                                .setContentText(msg)
                                                .show();
                                    }
                                });
                                view.postDelayed(new Runnable() { public void run() { view.setVisibility(View.INVISIBLE);
                                    dialog.dismiss();
                                    onBackPressed();
                                } }, 2000);
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(String error, int nCode) {
                            view.postDelayed(new Runnable() { public void run() {
                                new SweetAlertDialog(ProfileActivity.this, SweetAlertDialog.PROGRESS_TYPE)
                                        .setTitleText("Error!")
                                        .setContentText("Token Changed")
                                        .show();
                                dialog.dismiss();
                                Intent parentActivityIntent = new Intent(ProfileActivity.this, LoginActivity.class);
                                parentActivityIntent.addFlags(
                                        Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                                Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(parentActivityIntent);
                                finish();

                            } }, 100);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });
    }
}
