package com.example.ruby.arstan_nfc.utils;

import org.json.JSONObject;

/**
 * Created by jina on 10/24/2017.
 */

public class Listdata {
    public String name;
    public String action;
    public String gender;
    public String date;

    public Listdata(JSONObject json){
        name = json.optString("name", "");
        action = json.optString("action", "");
        gender = json.optString("gender", "");
        date = json.optString("date", "");
    }
}
