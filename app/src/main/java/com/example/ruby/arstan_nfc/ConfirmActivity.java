package com.example.ruby.arstan_nfc;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.PrintWriter;
import java.io.StringWriter;

import de.hdodenhof.circleimageview.CircleImageView;

public class ConfirmActivity extends AppCompatActivity {

    private Toolbar toolbar;
    NFCApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);

        try {

            application = (NFCApplication) getApplication();

            Intent intent = getIntent();

            String tf_name = intent.getStringExtra("name");
            String photo = intent.getStringExtra("photo");
            String classname = intent.getStringExtra("class");

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            CircleImageView image = (CircleImageView) findViewById(R.id.confirm_image);
            TextView name = (TextView) findViewById(R.id.confirm_name);
            name.setText(tf_name);
            TextView tv_class = (TextView) findViewById(R.id.confirm_class);
            tv_class.setText(classname);
            int lenth = application.logindata.api.length();
            String api = application.logindata.api.substring(0, lenth - 4);
            String url = String.format("%s%s%s", api, "/uploads/", photo);
            Picasso.with(this).load(url).into(image);

            new CountDownTimer(1500, 1000) {

                public void onTick(long millisUntilFinished) {
                    //                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    //                mTextField.setText("done!");
                    finish();
                }
            }.start();

        }catch (final Exception e){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    e.printStackTrace(pw);
                    String sStackTrace = sw.toString(); // stack trace as a string

                    Toast.makeText(ConfirmActivity.this, sStackTrace, Toast.LENGTH_LONG).show();

                }
            });
        }
    }
}
