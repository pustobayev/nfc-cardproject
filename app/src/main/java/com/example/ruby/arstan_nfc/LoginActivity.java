package com.example.ruby.arstan_nfc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.ruby.arstan_nfc.utils.Logindata;
import com.google.gson.Gson;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity {
    SharedPreferences sharedPref;

    String logined = "false";
    String login_state = "false";
    String auth;
    private String array_spinner[];
    Spinner sp_collagename;
    ArrayAdapter adapter;
    NFCApplication nfcApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        nfcApplication = (NFCApplication) getApplication();


        sharedPref = this.getSharedPreferences("shared", Context.MODE_PRIVATE);
        login_state = sharedPref.getString("login_state", "false");
        String access_ =  sharedPref.getString("abc", "false");
        logined = sharedPref.getString("logined", "false");
        if (logined.equals("true")){
            Gson gson = new Gson();
            String json = sharedPref.getString("logindata", "");
            Logindata log = gson.fromJson(json, Logindata.class);
            nfcApplication.logindata = log;

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        if(login_state.equals("true"))
        {
            Intent i = new Intent(LoginActivity.this,MainActivity.class);
            i.putExtra("access_token",access_);
            startActivity(i);
        }


        final EditText et_phonenumber = (EditText)findViewById(R.id.et_phonenumber);
        final EditText et_password = (EditText)findViewById(R.id.et_password);
        sp_collagename = (Spinner) findViewById(R.id.sp_collagename);

        sp_collagename.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int i = 0;
                return false;
            }
        });

//        array_spinner=new String[5];
//        array_spinner[0]="option 1";
//        array_spinner[1]="option 2";
//        array_spinner[2]="option 3";
//        array_spinner[3]="option 4";
//        array_spinner[4]="option 5";
//        ArrayAdapter adapter = new ArrayAdapter(this,
//                android.R.layout.simple_spinner_item, array_spinner);
//
//        sp_collagename.setAdapter(adapter);

//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent);

        APIManager.getInstance(this).getSchools(new APIManager.MyCallBackInterface() {
            @Override
            public void onSuccess(JSONObject result) {
                array_spinner = new String[nfcApplication.schools.size()+1];
                array_spinner[0] = "выберите школу";
                for (int i = 0; i < nfcApplication.schools.size(); i ++){
                    array_spinner[i+1] = nfcApplication.schools.get(i).name;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter = new ArrayAdapter(LoginActivity.this,
                                android.R.layout.simple_spinner_item, array_spinner);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sp_collagename.setAdapter(adapter);
                    }
                });
            }

            @Override
            public void onFailure(String error, int nCode) {
                int i = 0;
            }
        });

        Button btn_signin = (Button) findViewById(R.id.btn_signin);

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pose = sp_collagename.getSelectedItemPosition();
                final String url;
                if (pose == 0){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
//                                    .setTitleText("Error!")
                                    .setTitleText("ошибка!")
//                                    .setContentText("Input Correct")
                                    .setContentText("неправильный логин или пароль")
                                    .show();
                        }
                    });
                    return;
                }
                url = nfcApplication.schools.get(pose-1).api;
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("schoolname", nfcApplication.schools.get(pose-1).name);
                editor.putString("logo", nfcApplication.schools.get(pose-1).logo);
                editor.commit();

                APIManager.getInstance(LoginActivity.this).login(et_phonenumber.getText().toString(), et_password.getText().toString(),url, new APIManager.MyCallBackInterface() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        int i = 0;
                        Logindata login = new Logindata(result, url);

                        SharedPreferences.Editor editor = sharedPref.edit();
                        Gson gson = new Gson();
                        String json = gson.toJson(login);
                        editor.putString("logindata", json);

                        editor.putString("logined", "true");
                        editor.commit();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);


                    }

                    @Override
                    public void onFailure(String error, int nCode) {
                        runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Error!")
                                            .setContentText("Input Correct!")
                                            .show();
                                }
                            });
                    }
                });
//                    APIManager.getInstance(LoginActivity.this).getAccess_Token(et_phonenumber.getText().toString(), et_password.getText().toString(), new APIManager.MyCallBackInterface() {
//                        @Override
//                        public void onSuccess(JSONObject result) {
//                            try{
//                                auth = result.getString("access_token");
//                                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
//                                intent.putExtra("access_token",auth);
//
//                                Vibrator v = (Vibrator) getSystemService(MainActivity.VIBRATOR_SERVICE);
//
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//
////                                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE)
////                                                .setTitleText("Welcome!")
////                                                .setContentText("Success!")
////                                                .show();
//                                        SharedPreferences.Editor editor = sharedPref.edit();
//                                        editor.putString(getString(R.string.login_state), "true");
//                                        editor.putString(getString(R.string.access_token), auth);
//                                        editor.commit();
//                                    }
//                                });
//                                v.vibrate(1000);
//                                startActivity(intent);
//                            }catch (JSONException e){
//                                e.printStackTrace();
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(String error, int nCode) {
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//
//                                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
//                                            .setTitleText("Error!")
//                                            .setContentText("Try Again!")
//                                            .show();
//                                }
//                            });
//                        }
//                    });
            }
        });

    }
}
