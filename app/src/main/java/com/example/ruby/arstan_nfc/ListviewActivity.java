package com.example.ruby.arstan_nfc;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ruby.arstan_nfc.adapter.InOutStatusAdapter;
import com.example.ruby.arstan_nfc.utils.Listdata;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class ListviewActivity extends AppCompatActivity {

    private Toolbar toolbar;
    SharedPreferences sharedPref;
    InOutStatusAdapter adapter;
    ListView listView;
    ArrayList<Listdata> data = new ArrayList<Listdata>();
    int index = 0;
    boolean flag_loading;
    boolean flag_end;
//    TextView title;
//    ImageButton btn_prev;
//    ImageButton btn_next;
//    public int pageCount;
//    public int increment = 0;
//    public int TOTAL_LIST_ITEMS = 0;
//    public int NUM_ITEMS_PAGE = 2;
//    int start = 1;
//    int count = NUM_ITEMS_PAGE;

    NFCApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        application = (NFCApplication) getApplication();
        sharedPref = this.getSharedPreferences("shared", Context.MODE_PRIVATE);


        TextView day = (TextView) findViewById(R.id.list_day);

        String currentday = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        day.setText(currentday);

        //------ set Menu view
        TextView schoolname = (TextView) findViewById(R.id.school_name);
        schoolname.setText(sharedPref.getString("schoolname", ""));

        CircleImageView image = (CircleImageView) findViewById(R.id.school_image);
        int lenth = application.logindata.api.length();
        String api = application.logindata.api.substring(0, lenth - 4);
        String photo = sharedPref.getString("logo", "");
        String url = String.format("%s%s%s", api, "/assets/img/", photo);
        Picasso.with(this).load(url).into(image);
        //-------------//

        LinearLayout home = (LinearLayout) findViewById(R.id.layer_home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(ListviewActivity.this, MainActivity.class);
//                startActivity(intent);
                finish();
            }
        });
        listView = (ListView) findViewById(R.id.listview);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {


            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0)
                {
                    if(flag_loading == false)
                    {
                        flag_loading = true;
                        loadData();
//
                    }
                }
            }
        });
//        title = (TextView) findViewById(R.id.list_title);
//        title.setText(1+"-"+NUM_ITEMS_PAGE);
//        btn_prev = (ImageButton) findViewById(R.id.btn_pre);
//        btn_next = (ImageButton) findViewById(btn_next);
//        btn_prev.setEnabled(false);
//
//        btn_next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                if ((increment + 1) == pageCount){
////
////                    return;
////                }
//                increment ++;
//                loadList(increment);
//                CheckEnable();
//            }
//        });
//
//        btn_prev.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                increment --;
//                loadList(increment);
//                CheckEnable();
//            }
//        });


        adapter = new InOutStatusAdapter(getApplicationContext(), data);
        listView.setAdapter(adapter);

        String currentTimeString = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        APIManager.getInstance(this).getListData("2017-09-22", currentTimeString, new APIManager.MyCallBackInterface() {
            @Override
            public void onSuccess(JSONObject result) {
                int i =0;
//                TOTAL_LIST_ITEMS = application.listdatas.size();
//                int val = TOTAL_LIST_ITEMS % NUM_ITEMS_PAGE;
//                val = val==0?0:1;
//                pageCount = TOTAL_LIST_ITEMS/NUM_ITEMS_PAGE + val;
//                if (pageCount == 1 || pageCount == 0)
//                    btn_next.setEnabled(false);
//                if (pageCount < 2){
//                    count = TOTAL_LIST_ITEMS;
//                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        adapter = new InOutStatusAdapter(getApplicationContext(), data);
//                        listView.setAdapter(adapter);
                        loadData();
//                        adapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFailure(String error, int nCode) {
                int i = 0;
            }
        });


    }

    public void loadData(){
        for (int i = 0; i < 10; i ++){
            if (index < application.listdatas.size()){
                data.add(application.listdatas.get(index));
                index ++;
                flag_end = false;
            }else{
                break;
            }
        }
        if (index <= application.listdatas.size() && !flag_end)
        {
            if (index == application.listdatas.size())
                flag_end = true;
            flag_loading = false;
            adapter.notifyDataSetChanged();
        }

    }

//    public void CheckEnable(){
//        if ((increment +1)  == pageCount){
//            btn_next.setEnabled(false);
//        }else if (increment == 0){
//            btn_prev.setEnabled(false);
//        }else{
//            btn_prev.setEnabled(true);
//            btn_next.setEnabled(true);
//        }
//    }
//
//    public void loadList(int number){
//        title.setText((increment*NUM_ITEMS_PAGE+1)+"-"+((increment+1)*NUM_ITEMS_PAGE));
//
//        start = increment*NUM_ITEMS_PAGE+1;
//        if ((increment+1) == pageCount){
//            count = (TOTAL_LIST_ITEMS - increment*NUM_ITEMS_PAGE);
//        }else{
//            count = NUM_ITEMS_PAGE;
//        }
//
//        adapter = new InOutStatusAdapter(getApplicationContext(), start, 2, count);
//        listView.setAdapter(adapter);
//
//    }
}
