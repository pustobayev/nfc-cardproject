package com.example.ruby.arstan_nfc;

import android.app.Application;

import com.example.ruby.arstan_nfc.utils.Listdata;
import com.example.ruby.arstan_nfc.utils.Logindata;
import com.example.ruby.arstan_nfc.utils.School;

import java.util.ArrayList;

/**
 * Created by jina on 10/23/2017.
 */

public class NFCApplication extends Application {
    public ArrayList<School> schools;
    public Logindata logindata;
    public ArrayList<Listdata> listdatas;
    public int action = 1;
    @Override
    public void onCreate() {
        super.onCreate();

    }
}
